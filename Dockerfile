FROM alterway/php:5.3-apache
MAINTAINER Martin Helmich <typo3@martin-helmich.de>

RUN apt-get update &&\
    apt-get install -y --no-install-recommends \
# Configure PHP
        libpng12-dev \
        zlib1g-dev \
# Install required 3rd party tools
        graphicsmagick && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install mysql mysqli gd && \
    echo 'upload_max_filesize = 32M\npost_max_size = 32M' > /usr/local/etc/php/conf.d/fileupload.ini && \
# Configure Apache as needed
    apt-get clean && \
    apt-get -y purge \
        libpng12-dev \
        zlib1g-dev && \
    rm -rf /var/lib/apt/lists/* /usr/src/*
